# Temat
Test-driven development/TDD.

# Cel
Celem zajęć jest przedstawienie metodyki Test-driven development, z wykorzystaniem bibliotek do testowania jednostkowego. W ramach zajęć zostaną wykonane ćwiczenia polegające na tworzeniu fragmentów oprogramowania zgodnie z metodyką TDD.

# Technologie
*  Java 8, JUnit 5.x
*  Python 3.x, pytest, unittest
*  Angular 7.x, typescript

## Wprowadzenie
TDD, Test Driven Development, to proces tworzenia oprogramowania sterowany testami. Tworzenie nowej funkcjonalności jest poprzedzone utworzeniem testu, który powinien przejść dopiero jak właściwa funkcjonalność zostanie utworzona. W momencie tworzenia testu najczęściej klasa testowana (lub inna forma kodu) lub metody testowane jeszcze nie istnieją, więc tak stworzony kod nawet się nie skompiluje.

## Zadanie
Należy uzupełnić funkcjonalność istniejącego systemu o wyspecyfikowane elementy. Zmiany należy wprowadzać zgodnie z TDD - tj. rozpoczynamy pracę od przemyślenia funkcjonalności, napisania testów i uzupełnienia kodu biznesowego. Do wyboru jest kilka wariantów, które dotyczą projektu ze stosem i translatorem, warianty mogą dotyczyć wybranych technologii.

Projekty ze stosem: https://gitlab.com/spio-sources/stack

Projekty z translatorem: https://gitlab.com/spio-sources/translator

**W każdym projekcie przewidziano pusty plik na testy TDD, który stanowi punkt początkowy zadań.**

### Wariant 1, szyfr cezara, translator
https://pl.wikipedia.org/wiki/Szyfr_Cezara

Należy napisać transltor implementujący szyfr cezara, czyli prosty szyfr podstawieniowy z przesunięciem.

Dla zaawansowanych: w wybranej technologii należy wyciągnąć ogólny interfejs translatora z gaderypoluki i dodać implementację szyfru cezara. Uwaga na projekty w Javie i Angularze, które korzystają ze wstrzykiwania zależności.

### Wariant 2, translacja case sensitive, translator
Należy uzupełnić translator gaderypoluki o funkcjonalność rozpoznawania/respektowania wielkości znaków.

### Wariant 3, kopia danych, stos
Należy uzupełnić stos o funkcjonalność, która zwróci kopię zawartości stosu. W pierwszej kolejności należy zrozumieć sposób przechowywania wartości w danej technologii.

### Wariant 4, kopia danych reversed, stos
Należy uzupełnić stos o funkcjonalność, która zwróci kopię zawartości stosu, w odwróconym porządku. Ograniczenia jak w wariancie powyższym.
